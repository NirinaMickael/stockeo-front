import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = 
[
  {
    path:'',
    loadChildren : ()=>import('./pages/page/page.module').then(e=>e.PageModule)
  },
  {
    path:'auth',
    loadChildren : ()=>import('./pages/auth/auth.module').then(e=>e.AuthModule)
  },
  {
    path:'**',
    redirectTo:'auth'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
