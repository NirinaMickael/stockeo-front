import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientListComponent } from './client/client-list/client-list.component';
import { LayoutComponent } from 'src/app/layout/layout.component';

const routes: Routes = [
  {
    path:'',
    component:LayoutComponent,
    children: [
      {
        path:'',
        redirectTo:'users',
        pathMatch:'full'
      },
       {
        path:'users',
        loadChildren:()=>import('./client/client.module').then(e=>e.ClientModule),
        component:ClientListComponent
       }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }
