import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageRoutingModule } from './page-routing.module';
import { LayoutModule } from 'src/app/layout/layout.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PageRoutingModule,
    LayoutModule
  ]
})
export class PageModule { }
