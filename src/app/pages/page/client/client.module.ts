import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientListComponent } from './client-list/client-list.component';
import { ClientAddComponent } from './client-add/client-add.component';

@NgModule({
  declarations: [
    ClientListComponent,
    ClientAddComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ClientModule { }
